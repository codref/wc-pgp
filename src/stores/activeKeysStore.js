import { defineStore, createPinia, setActivePinia } from "pinia";
import * as openpgp from "openpgp"

setActivePinia(createPinia());

export const useActiveKeysStore = defineStore('activeKeys', {
  state: () => ({
    privateKey: null,
    privateKeyStatus: "missing",
    privateKeyError: "",
    publicKey: null,
    publicKeyStatus: "missing",
    publicKeyError: ""
  }),

  actions: {
    setPrivateKey(key) {
      this.privateKey = key
      this.privateKeyStatus = "ok"
      this.privateKeyError = ""
    },
    setPublicKey(key) {
      this.publicKey = key
      this.publicKeyStatus = "ok"
      this.publicKeyError = ""
    },
    async toLocalStorage(storePrivateKey, storePublicKey){
      const keys = []
      if (storePrivateKey && this.privateKey != null) {
        keys.push(this.privateKey)
      }
      if (storePublicKey && this.publicKey != null) {
        keys.push(this.publicKey)
      }
      if (keys.length == 0) {
        throw new Error("Cannot store keys on browser [no key found]")
      }

      for (const key of keys) {
        const id = key.getKeyID().toHex()
        const common = {
          id: id,
          label: `${key.getUserIDs()[0]} [0x${id}]`,
          fingerprint: key.getFingerprint(),
          created: key.keyPacket.created,
          expires: await key.getExpirationTime(),
          algorithm: key.getAlgorithmInfo(),
          userIDs: key.getUserIDs(),
        }
        if (key.isPrivate()) {
          var privateKeys = JSON.parse(localStorage.getItem("stored-keys.private"))
          if (privateKeys == null) {
            privateKeys = []
          }
          privateKeys.push({
            privateKey: key.armor(),
            encrypted: !key.isDecrypted(),
            ...common
          })
          localStorage.setItem("stored-keys.private", JSON.stringify(privateKeys))
        } else {
          var publicKeys = JSON.parse(localStorage.getItem("stored-keys.public"))
          if (publicKeys == null) {
            publicKeys = []
          }
          publicKeys.push({
            publicKey: key.armor(),
            ...common
          })
          localStorage.setItem("stored-keys.public", JSON.stringify(publicKeys))
        } 
      }    
    },
    async setPublicAAKey(armoredKey) {
      if (armoredKey == "") {
        this.publicKey = null
        this.publicKeyStatus = "missing"
      } else {
        try {
          const key = await openpgp.readKey({
            armoredKey: armoredKey,
          })
          if (!key.isPrivate()) {
            this.publicKey = key
            this.publicKeyStatus = "ok"
          } else {
            this.publicKey = null
            this.publicKeyStatus = "wrong"
          }
        } catch (e) {
          this.publicKey = null
          this.publicKeyStatus = "error"
          this.publicKeyError = e.toString()
        }
      }      
    },
    async setPrivateAAKey(armoredKey) {
      if (armoredKey == "") {
        this.privateKey = null
        this.privateKeyStatus = "missing"
      } else {
        try {
          const key = await openpgp.readKey({
            armoredKey: armoredKey,
          })
          if (key.isPrivate()) {
            this.privateKey = key
            this.privateKeyStatus = "ok"
          } else {
            this.privateKey = null
            this.privateKeyStatus = "wrong"
          }
        } catch (e) {
          this.privateKey = null
          this.privateKeyStatus = "error"
          this.privateKeyError = e.toString()
        }
      }   
    }
  },
});