import { defineStore, createPinia, setActivePinia } from "pinia"

setActivePinia(createPinia())

export const useKeysStore = defineStore("keys", {
  state: () => ({
    counter: 0,
    keys: [],
    pendingChanges: false,
  }),

  actions: {
    async add(publicKey, privateKey) {
      const key = privateKey !== null ? privateKey : publicKey
      const expires = await key.getExpirationTime()
      this.keys.unshift({
        id: key.getKeyID().toHex(),
        label: `${key.getUserIDs()[0]} [0x${privateKey.getKeyID().toHex()}]`,
        privateKey: privateKey !== null ? privateKey.armor() : null,
        publicKey: publicKey !== null ? publicKey.armor() : null,
        fingerprint: key.getFingerprint(),
        algorithm: key.getAlgorithmInfo(),
        created: key.keyPacket.created.toISOString(),
        expires: expires instanceof Date ? expires.toISOString() : expires,
        encrypted: privateKey != null ? !privateKey.isDecrypted() : false,
        userIDs: key.getUserIDs(),
        isPublicKeyDeleted: false,
        isPrivateKeyDeleted: false,
        isEphemeral: true,
      })
      this.counter++
      this.pendingChanges = true
    },
    fromLocalStorage() {
      var storedKeys = {}
      if (localStorage.getItem("stored-keys.private")) {
        const privateKeys = JSON.parse(
          localStorage.getItem("stored-keys.private")
        )
        for (const key of privateKeys) {
          storedKeys[key.label] = {
            id: key.id,
            label: key.label,
            privateKey: key.privateKey,
            publicKey: null,
            fingerprint: key.fingerprint,
            created: key.created,
            expires: key.expires,
            encrypted: key.encrypted,
            algorithm: key.algorithm,
            userIDs: "userIDs" in key ? Array.isArray(key.userIDs) ? key.userIDs.join(", ") : key.userIDs : undefined,
            isPublicKeyDeleted: false,
            isPrivateKeyDeleted: false,
            isEphemeral: false,
          }
        }
      }
      if (localStorage.getItem("stored-keys.public")) {
        const publicKeys = JSON.parse(
          localStorage.getItem("stored-keys.public")
        )
        for (const key of publicKeys) {
          if (key.label in storedKeys) {
            // if private key is already present
            storedKeys[key.label].publicKey = key.publicKey
          } else {
            storedKeys[key.label] = {
              id: key.id,
              label: key.label,
              publicKey: key.publicKey,
              privateKey: null,
              fingerprint: key.fingerprint,
              created: key.created,
              expires: key.expires,
              algorithm: key.algorithm,
              userIDs: "userIDs" in key ? Array.isArray(key.userIDs) ? key.userIDs.join(", ") : key.userIDs : undefined,
              isPublicKeyDeleted: false,
              isPrivateKeyDeleted: false,
              isEphemeral: false,
            }
          }
        }
      }
      this.keys = Object.values(storedKeys)
      this.counter = this.keys.length
      this.pendingChanges = false
    },
    toLocalStorage() {
      const publicKeys = [],
        privateKeys = []
      for (const key of this.keys) {
        const common = {
          id: key.id,
          label: key.label,
          fingerprint: key.fingerprint,
          created: key.created,
          expires: key.expires,
          algorithm: key.algorithm,
          userIDs: key.userIDs,
        }
        if (key.publicKey != null && !key.isPublicKeyDeleted) {
          publicKeys.push({
            publicKey: key.publicKey,
            ...common,
          })
        }
        if (key.privateKey != null && !key.isPrivateKeyDeleted) {
          privateKeys.push({
            privateKey: key.privateKey,
            ...common,
          })
        }
      }
      localStorage.setItem("stored-keys.public", JSON.stringify(publicKeys))
      localStorage.setItem("stored-keys.private", JSON.stringify(privateKeys))
      this.pendingChanges = false
    },    
  },
})
