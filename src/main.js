import wrap from '@vue/web-component-wrapper';
import Vue from 'vue';
import App from './App.vue';

const wrappedElement = wrap(Vue, App);
Vue.config.productionTip = false;

window.customElements.define('web-app', wrappedElement);