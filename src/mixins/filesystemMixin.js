export default {
  methods: {
    b64toBlob(b64Data, contentType = "", sliceSize = 512) {
      const byteCharacters = atob(b64Data)
      const byteArrays = []

      for (
        let offset = 0;
        offset < byteCharacters.length;
        offset += sliceSize
      ) {
        const slice = byteCharacters.slice(offset, offset + sliceSize)

        const byteNumbers = new Array(slice.length)
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i)
        }

        const byteArray = new Uint8Array(byteNumbers)
        byteArrays.push(byteArray)
      }

      const blob = new Blob(byteArrays, { type: contentType })
      return blob
    },
    formatBytes(bytes, decimals = 2) {
      if (!+bytes) return "0 Bytes"

      const k = 1024
      const dm = decimals < 0 ? 0 : decimals
      const sizes = [
        "Bytes",
        "KiB",
        "MiB",
        "GiB",
        "TiB",
        "PiB",
        "EiB",
        "ZiB",
        "YiB",
      ]

      const i = Math.floor(Math.log(bytes) / Math.log(k))

      return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
    },
    downloadFromBrowser(filename, data, contentType = "text/plain") {
      var element = document.createElement("a")
      const base64HeaderIndex = data.indexOf("base64,")
      if (base64HeaderIndex > 0) {
        element.setAttribute("href", data)
      } else {
        element.setAttribute(
          "href",
          `data:${contentType};charset=utf-8,${encodeURIComponent(data)}`
        )
      }
      element.setAttribute("download", filename)
      element.style.display = "none"
      document.body.appendChild(element)
      element.click()
      document.body.removeChild(element)
    },
    copyToClipboard(el) {
      if (typeof el.select !== "undefined") {
        el.select()
      } else {
        el.querySelector("textarea").select()
      }
      document.execCommand("copy")
      this.$emit(
        "notify",
        `Text copied to clipboard!`,
        "is-info is-light",
        3000
      )
    },
  },
}
